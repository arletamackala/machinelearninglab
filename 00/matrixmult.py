#! /usr/bin/env python3

import pytest
import numpy as np

def multiply(a, b):
    if len(a)>0 and len(b)>0:
        m=len(a[0])
        n=len(a)
        l=len(b[0])
        k=len(b)
        #we check if matrices are properly defined
        for i in range(0,len(a)):
            if m!= len(a[i]):
                raise Exception
        for i in range(0,len(b)):
            if l!= len(b[i]):
                raise Exception
        if m==k:
            C = np.zeros((n,l))
            for i in range(n):
                for j in range(l):
                    liczba=0
                    for s in range(m):
                        liczba += a[i][s]*b[s][j]
                    C[i][j] = liczba
            return C
        raise Exception



def multiplyWithNumpy(a, b):
    return np.dot(a, b)


testdata = [
    ([[3]], [[6]], [[18]]),
    ([[1, 2]],   [[3], [4]], [[11]]),
    ([[1, 2, 3], [4, 5, -7]],  [[3, 0, 4], [2, 5, 1], [-1, -1, 0]],  [[4, 7, 6], [29, 32, 21]])
]
@pytest.mark.parametrize("multiplicationFunc", [multiply, multiplyWithNumpy])
@pytest.mark.parametrize("a,b,expected", testdata)
def test_multiply(multiplicationFunc, a, b, expected):
    assert np.array_equal(multiplicationFunc(a, b), expected)


testdataImpossibleProducts = [
    ([[3, 4]], [[6, 7]]),
    ([[1, 2]], [[3, 4], [4, 5], [5, 6]])
]
@pytest.mark.parametrize("multiplicationFunc", [multiply, multiplyWithNumpy])
@pytest.mark.parametrize("a,b", testdataImpossibleProducts)
def test_multiplyIncompatibleMatrices(multiplicationFunc, a, b):
    with pytest.raises(Exception) as e:
        multiplicationFunc(a, b)



